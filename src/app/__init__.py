import os

import connexion

from config import Config, basedir

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


# Create the application instance
connexion_app = connexion.FlaskApp(__name__)
application = connexion_app.app
application.config.from_object(Config)

# Read the swagger.yml file to configure the endpoints
connexion_app.add_api(Config.SWAGGER_FILE)

# Configuration for database
db = SQLAlchemy(application)
migrate = Migrate(application, db)

from app import models

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    connexion_app.run(host='0.0.0.0', port=5000, debug=True)