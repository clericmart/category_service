import os

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'my secret pass'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SWAGGER_FILE = os.path.join(basedir, 'swagger.yml')
    SQLALCHEMY_TRACK_MODIFICATIONS = False