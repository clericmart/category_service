import itertools
import json
import app

from collections import defaultdict
from utils import get_tokens
from datetime import datetime
from flask import (make_response, abort)


def get_formated_category(sentence):
    matcher = set(tuple(x.keywords.split(",")) for x in app.models.Matcher.query.all())
    combinations = itertools.combinations(get_tokens(sentence), 2)
    intersection = matcher.intersection(combinations)
    if intersection:
        intersection = intersection.pop()
        return app.models.Matcher.query.filter_by(keywords="{},{}".format(intersection[0], intersection[1])).first().formated_category


def format(*args, **kwargs):
    """
    This function format category into Intu category
    :param unformated_category:  category that will be formated
    :return:        201 on success, 406 on can't match category with intu
    """
    text = kwargs.get('unformated_category', None)
    formated_category = get_formated_category(unicode(text.lower()))
    if formated_category is None:
        return make_response("Can't match with any category", 406)
    else:
        return make_response(formated_category, 201)

def format_batch(*args, **kwargs):
    """
    This function format batch of categories into Intu category
    :param categories_to_format:  categories that will be formated
    :return:        201 on success, with application/json of matched dict
    """
    categories = kwargs.get("categories_to_format", None)
    response = defaultdict(str)
    for cat in categories:
        formated_category = get_formated_category(unicode(cat.lower()))
        if formated_category:
            response[cat] = formated_category
        else:
            response[cat] = ""
    return make_response(json.dumps(response), 201)
