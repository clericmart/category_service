from app import db

class Category(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)
    alias = db.Column(db.String(120), index = True, unique = True)
    similiar_names = db.relationship('Similiar', backref = 'category')
    sub_category = db.relationship('SubCategory', backref = 'category')

    def __repr__(self):
        return '<Category %r>' % (self.name)


class SubCategory(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True)
    alias = db.Column(db.String(120), index = True, unique = True)
    sub_similiar_names = db.relationship('SubSimiliar', backref = 'subcategory')
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))

    def __repr__(self):
        return '<SubCategory %r>' % (self.name)


class Similiar(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))

    def __repr__(self):
        return '<Similiar Category %r>' % (self.name)


class SubSimiliar(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)
    sub_category_id = db.Column(db.Integer, db.ForeignKey('sub_category.id'))

    def __repr__(self):
        return '<Similiar SubCategory %r>' % (self.name)


class Matcher(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    keywords = db.Column(db.String(120), index = True)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    sub_category_id = db.Column(db.Integer, db.ForeignKey('sub_category.id'))
    formated_category = db.Column(db.String(120))

    def __repr__(self):
        return '<Matcher %r>' % (self.keywords)
