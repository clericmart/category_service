#!/usr/bin/env python
import os
from migrate.versioning import api as migrate_api
from config import Config
from app import db

db.create_all()
print(Config.SQLALCHEMY_MIGRATE_REPO)
if not os.path.exists(Config.SQLALCHEMY_MIGRATE_REPO):
    migrate_api.create(Config.SQLALCHEMY_MIGRATE_REPO, 'database repository')
    migrate_api.version_control(
        Config.SQLALCHEMY_DATABASE_URI,
        Config.SQLALCHEMY_MIGRATE_REPO
    )
else:
    migrate_api.version_control(
        Config.SQLALCHEMY_DATABASE_URI, 
        Config.SQLALCHEMY_MIGRATE_REPO, 
        migrate_api.version(Config.SQLALCHEMY_MIGRATE_REPO)
    )