# This file should download first page of intu and split categories
import requests
import json

from collections import defaultdict
from bs4 import BeautifulSoup

category_dict = defaultdict(dict)

with open('test.html', 'r') as input_file:
    soup = BeautifulSoup(input_file.read())
    tabs_content = soup.find('div', {'class': 'tabs-content vertical'})
    contents = tabs_content.findAll('div', {'class': 'megamenu__content'})
    for key, category in enumerate(contents):
        cat = category.find('a', {'class': 'megamenu__content--title'})
        cat_name = cat['data-ga-label']
        if cat_name in ["Collection services", "Shop by store", "Office & stationery", "Gifts", "Sports & leisure"]:
            continue
        category_dict[cat_name]['alias'] = cat['href'].split('/')[-1]
        sub_cat_list = []
        for sub_cat in category.findAll('li'):
            sub_cat_ = sub_cat.find('a')
            sub_name = sub_cat_.text
            if sub_name in ["View all", "View All"]:
                continue
            sub_cat_list.append({
                "name": sub_cat_.text,
                "alias": sub_cat_['href'].split('/')[-1]
            })
        category_dict[cat['data-ga-label']]['subcategory'] = sub_cat_list

with open('intu_category.json', 'w') as output_file:
    json.dump(category_dict, output_file)
