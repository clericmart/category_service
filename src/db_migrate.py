#!/usr/bin/env python
import imp
from migrate.versioning import api as migrate_api
from app import db
from config import Config
migration = Config.SQLALCHEMY_MIGRATE_REPO + '/versions/%03d_migration.py' % (
    migrate_api.db_version(Config.SQLALCHEMY_DATABASE_URI, Config.SQLALCHEMY_MIGRATE_REPO) + 1
)
tmp_module = imp.new_module('old_model')
old_model = migrate_api.create_model(Config.SQLALCHEMY_DATABASE_URI, Config.SQLALCHEMY_MIGRATE_REPO)
exec old_model in tmp_module.__dict__
script = migrate_api.make_update_script_for_model(
    Config.SQLALCHEMY_DATABASE_URI,
    Config.SQLALCHEMY_MIGRATE_REPO,
    tmp_module.meta, 
    db.metadata
)
open(migration, "wt").write(script)
migrate_api.upgrade(Config.SQLALCHEMY_DATABASE_URI, Config.SQLALCHEMY_MIGRATE_REPO)
print 'New migration saved as ' + migration
print 'Current database version: ' + str(
    migrate_api.db_version(Config.SQLALCHEMY_DATABASE_URI, Config.SQLALCHEMY_MIGRATE_REPO)
)