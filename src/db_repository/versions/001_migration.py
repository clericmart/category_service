from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
category = Table('category', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('alias', String(length=120)),
)

matcher = Table('matcher', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('keywords', String(length=120)),
    Column('category_id', Integer),
    Column('sub_category_id', Integer),
    Column('formated_category', String(length=120)),
)

similiar = Table('similiar', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('category_id', Integer),
)

sub_category = Table('sub_category', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('alias', String(length=120)),
    Column('category_id', Integer),
)

sub_similiar = Table('sub_similiar', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('sub_category_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['category'].create()
    post_meta.tables['matcher'].create()
    post_meta.tables['similiar'].create()
    post_meta.tables['sub_category'].create()
    post_meta.tables['sub_similiar'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['category'].drop()
    post_meta.tables['matcher'].drop()
    post_meta.tables['similiar'].drop()
    post_meta.tables['sub_category'].drop()
    post_meta.tables['sub_similiar'].drop()
