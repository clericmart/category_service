import jinja2

from app import connexion_app, application
from flask import render_template


my_loader = jinja2.ChoiceLoader([
        application.jinja_loader,
        jinja2.FileSystemLoader(['templates',]),
    ])
application.jinja_loader = my_loader

@application.route('/')
def home(name=None):
    return render_template('home.html', name=name)

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    connexion_app.run(host='0.0.0.0', port=5000, debug=True)