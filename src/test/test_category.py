import pytest
import connexion
import os
import json
import tempfile
import pytest
from flask_sqlalchemy import SQLAlchemy


@pytest.fixture(scope='module')
def client():
    basedir = os.path.abspath(os.path.dirname(__file__))
    connexion_app = connexion.App(__name__, specification_dir='../')
    flask_app = connexion_app.app
    connexion_app.add_api('swagger.yml')
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}/test.db'.format(basedir) 
    flask_app.config['TESTING'] = True
    db = SQLAlchemy(flask_app)
    client = flask_app.test_client()

    yield client


@pytest.mark.parametrize("test_input, expected", [
        ("Men's clothing", "/Men/Clothing"),
        ("adult - female - jewelry and watches - fashion jewelry", "/Women/Jewellery"),
    ])
def test_category(client, test_input, expected):
    response = client.post('/api/category', data=test_input, content_type="plain/text")
    assert response.data == expected
    assert response.status_code == 201


@pytest.mark.parametrize("test_input, expected", [
        ("test category", "Can't match with any category"),
    ])
def test_category_failed(client, test_input, expected):
    response = client.post('/api/category', data=test_input, content_type="plain/text")
    assert response.data == expected
    assert response.status_code == 406


@pytest.mark.parametrize("test_input, expected", 
    [(
        json.dumps(
            ["Mens clothing", "adult - female - jewelry and watches - fashion jewelry"]
        ),
        json.dumps({
            "Mens clothing": "/Men/Clothing",
            "adult - female - jewelry and watches - fashion jewelry": "/Women/Jewellery"
            }
        )
    ),]
)
def test_category_batch(client, test_input, expected):
    response = client.post('/api/category/batch', data=test_input, content_type="application/json")
    assert response.data == expected
    assert response.status_code == 201
