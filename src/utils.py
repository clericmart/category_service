# Utils functions
import json
import spacy
import itertools
import collections
import app


def normilize_to_intu_category(text):
    text = "adult - female - jewelry and watches - fashion jewelry"
    keywords = text.split(" ")
    print list(itertools.combinations(keywords, 2))
    pass


def find_similiar_words(word):
    print "Start proccessing of {}".format(word)
    nlp = spacy.load('en_core_web_md')
    def get_related(word):
        filtered_words = [w for w in word.vocab if w.is_lower == word.is_lower and w.prob >= -15]
        similarity = sorted(filtered_words, key=lambda w: word.similarity(w), reverse=True)
        return similarity[:15]
    return [w.lower_ for w in get_related(nlp.vocab[word])]


def get_tokens(sentence):
    """
    (token.text, token.lemma_, token.pos_,)
    """
    nlp = spacy.load('en_core_web_sm')
    doc = nlp(sentence)
    return (token.text for token in doc if token.pos_ not in ['PUNCT', 'CCONJ'])


def generate_matcher_table():
    catories = app.models.Category.query.all()
    for cat in catories:
        category_similiar = [x.name for x in cat.similiar_names]
        for subcat in cat.sub_category:
            if subcat.sub_similiar_names:
                sub_category_similiar = [x.name for x in subcat.sub_similiar_names]
            else:
                continue
            for prod in itertools.product(category_similiar, sub_category_similiar):
                missing = app.models.Matcher.query.filter_by(
                    keywords="{},{}".format(prod[0], prod[1]),
                    category_id=cat.id,
                    sub_category_id=subcat.id
                ).first()
                if missing is None:
                    u = app.models.Matcher(
                        keywords="{},{}".format(prod[0], prod[1]),
                        category_id=cat.id,
                        sub_category_id=subcat.id,
                        formated_category="/{}/{}".format(cat.name, subcat.name)
                    )
                    app.db.session.add(u)
                    app.db.session.commit()


def init_db():
    with open("./assets/intu_category_formated.json", "r") as read_file:
        data = json.load(read_file)
        for category in data:
            missing = app.models.Category.query.filter_by(name=category).first()
            category_id = None
            if missing is None:
                u = app.models.Category(name=category, alias=data[category]['alias'])
                app.db.session.add(u)
                app.db.session.commit()
                app.db.session.refresh(u)
                category_id = u.id
            else:
                category_id = missing.id
            print category_id, category, data[category]['alias']
            for subcat in data[category]['subcategory']:
                missing = app.models.SubCategory.query.filter_by(name=subcat['name'], category_id=category_id).first()
                if missing == None:
                    su = app.models.SubCategory(name=subcat['name'], alias=subcat['alias'], category_id=category_id)
                    app.db.session.add(su)
                    app.db.session.commit()
                print "\t", subcat['name'], subcat['alias']


def generate_similiar():
    some_dict = collections.defaultdict(list)
    for cat in app.models.Category.query.all():
        some_dict[cat.name] = find_similiar_words(cat.name)
    
    with open("./assets/category_similiar.json", "w") as output_file:
        json.dump(some_dict, output_file)
    
    print "End with category"
    
    sub_some_dict = collections.defaultdict(list)
    for sub in app.models.SubCategory.query.all():
        sub_some_dict[sub.name] = find_similiar_words(sub.name)
    
    with open("./assets/sub_category_similiar.json", "w") as output_file:
        json.dump(sub_some_dict, output_file)

    print "End with sub category"


def update_similiar():
    with open("./assets/category_similiar_formated.json", "r") as input_file:
        data = json.load(input_file)
        for cat in data:
            category = app.models.Category.query.filter_by(name=cat).first()
            if category is not None:
                for sim_cat in data[cat]:
                    missing = app.models.Similiar.query.filter_by(name=sim_cat, category_id=category.id).first()
                    if missing is None:
                        siu = app.models.Similiar(name=sim_cat, category_id=category.id)
                        app.db.session.add(siu)
                        app.db.session.commit()

    with open("./assets/sub_category_similiar_formated.json", "r") as input_file:
        data = json.load(input_file)
        for subcategory in app.models.SubCategory.query.all():
            try:
                subcatlist = data[subcategory.name]
            except Exception as e:
                print e.message
                continue
            for sim_cat in subcatlist:
                missing = app.models.SubSimiliar.query.filter_by(name=sim_cat, sub_category_id=subcategory.id).first()
                if missing is None:
                    siu = app.models.SubSimiliar(name=sim_cat, sub_category_id=subcategory.id)
                    app.db.session.add(siu)
                    app.db.session.commit()


if __name__ == "__main__":
    # init_db()
    # update_similiar()
    # generate_similiar()
    # generate_matcher_table()
    # print models.Matcher.query.filter_by(keywords="{},{}".format("female", "jewelry")).first()
    pass

