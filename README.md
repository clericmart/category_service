# Description

Simple service for matching categories from different resources into predefined categories

## Running on localhost:5000

## docker image build and run instructions
* Build base image
```bash
docker build -t ymartynenko/category_service .
```

* Run container with service
```bash
docker run --name test_cat --rm -it -p 5000:5000 ymartynenko/category_service:latest python server.py
```

* Run container for testing
```bash
docker run --name test_cat --rm -it -p 5000:5000 ymartynenko/category_service:latest python -m pytest
```

# Spent time
- Setup base database schema 1 hour
- Configure Swagger API 1 hour
- Find out tools for NLP like proccessing 2 hours
- Writing tests 1 hour
- Writing doc 1 hour
- Wrapping up with docker 1 hour
- Clean up 1 hour


# Things to improve:
- Make all security sensetive pass as env.var
- Writing healthcheck tests
- Separate db scripts into module
- Cover utils.py functions with unittest
- Add logger
- Make test database setup from fixtures
- Dump for database
- CI/CD setup
- Refactoring
- Split docker setUp into dev/prod
